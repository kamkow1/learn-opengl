// ----------------------------------------------------
// shader utility functions
// ----------------------------------------------------

#include <string>
#include <fstream>
#include <sstream>
#include <string.h>

#include "glad.h"
#include <GLFW/glfw3.h>

#include "Shader.h"
#include "logging.h"
#include "defer.h"

std::string slrup_file(const char* path)
{
    std::ifstream f(path);
    std::stringstream buffer;
    buffer << f.rdbuf();
    return buffer.str();
}

unsigned int create_shader_module(unsigned int type, const char* source_path)
{
    std::string source = slrup_file(source_path);
    const char* cstring_source = source.c_str();

    unsigned int id;
    id = glCreateShader(type);
    glShaderSource(id, 1, &cstring_source, 0);
    glCompileShader(id);

    int ok;
    char info_log[512];
    memset(info_log, 0, sizeof(info_log));
    glGetShaderiv(id, GL_COMPILE_STATUS, &ok);
    if (!ok) {
        glGetShaderInfoLog(id, sizeof(info_log), 0, info_log);
        LOG_ERROR("Failed to compile shader module %s:\n%s\n", source_path, info_log);
    }
    return id;
}

unsigned int create_shader_program(const char *vertex_path, const char *fragment_path)
{
    unsigned int vertex = create_shader_module(GL_VERTEX_SHADER, vertex_path);
    unsigned int fragment = create_shader_module(GL_FRAGMENT_SHADER, fragment_path);
    defer({
        glDeleteShader(vertex);        
        glDeleteShader(fragment);        
    });

    unsigned int id;
    id = glCreateProgram();
    glAttachShader(id, vertex);
    glAttachShader(id, fragment);
    glLinkProgram(id);

    int ok;
    char info_log[512];
    memset(info_log, 0, sizeof(info_log));
    glGetProgramiv(id, GL_LINK_STATUS, &ok);
    if (!ok) {
        glGetProgramInfoLog(id, sizeof(info_log), 0, info_log);
        LOG_ERROR("Failed to link shader %s\n", info_log);
    }

    return id;
}

// ----------------------------------------------------
// Shader
// ----------------------------------------------------

Shader::Shader(const char* vert, const char* frag)
{
    this->id = create_shader_program(vert, frag);
}
    
Shader::~Shader(void)
{
    glDeleteProgram(this->id);
}

void Shader::use(void)
{
    glUseProgram(this->id);
}

void Shader::set(const char* name, float f)
{
    glUniform1f(glGetUniformLocation(this->id, name), f);
}

void Shader::set(const char* name, double d)
{
    glUniform1f(glGetUniformLocation(this->id, name), (float)d);
}

void Shader::set(const char* name, bool b)
{
    glUniform1i(glGetUniformLocation(this->id, name), (int)b);
}

void Shader::set(const char* name, int i)
{
    glUniform1i(glGetUniformLocation(this->id, name), i);
}

