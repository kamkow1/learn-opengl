#ifndef LOGGING_H_
#define LOGGING_H_

#define LOG_ERROR(fmt, ...) printf("ERROR: " fmt, ##__VA_ARGS__)

#endif // LOGGING_H_
