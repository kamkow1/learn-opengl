#version 330 core

layout (location=0) in vec3 in_pos;
layout (location=1) in vec3 in_color;

out vec3 vert_color;

uniform float dt;

void main(void)
{
    float normalized_dt = abs(sin(dt));
    gl_Position = vec4(in_pos.x, in_pos.y * normalized_dt, in_pos.z, 1.0f);
    vert_color = in_color;

}

