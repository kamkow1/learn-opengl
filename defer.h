#ifndef DEFER_H_
#define DEFER_H_

// generic "defer" implementation for both C and C++

// DEFER

#ifdef __cplusplus
#include <utility>
// https://github.com/mattkretz/defer/blob/master/defer.h
namespace ext
{
namespace detail
{
template <typename F> class defer_raii
{
public:
  // copy/move construction and any kind of assignment would lead to the cleanup function getting
  // called twice. We can't have that.
  defer_raii(defer_raii &&) = delete;
  defer_raii(const defer_raii &) = delete;
  defer_raii &operator=(const defer_raii &) = delete;
  defer_raii &operator=(defer_raii &&) = delete;

  // construct the object from the given callable
  template <typename FF> defer_raii(FF &&f) : cleanup_function(std::forward<FF>(f)) {}

  // when the object goes out of scope call the cleanup function
  ~defer_raii() { cleanup_function(); }

private:
  F cleanup_function;
};
}  // namespace detail

template <typename F> detail::defer_raii<F> defer(F &&f)
{
  return {std::forward<F>(f)};
}

#define DEFER_ACTUALLY_JOIN(x, y) x##y
#define DEFER_JOIN(x, y) DEFER_ACTUALLY_JOIN(x, y)
#ifdef __COUNTER__
  #define DEFER_UNIQUE_VARNAME(x) DEFER_JOIN(x, __COUNTER__)
#else
  #define DEFER_UNIQUE_VARNAME(x) DEFER_JOIN(x, __LINE__)
#endif

#define defer(lambda__) [[maybe_unused]] const auto& DEFER_UNIQUE_VARNAME(defer_object) = ext::defer([&]() lambda__)
}  // namespace ext
#else
//https://github.com/cmhood/c-defer/blob/master/defer.h
#define defer defer__2(__COUNTER__)
#define defer__2(X) defer__3(X)
#define defer__3(X) defer__4(defer__id##X)
#define defer__4(ID) auto void ID##func(char (*)[]); __attribute__((cleanup(ID##func))) char ID##var[0]; void ID##func(char (*ID##param)[])

#endif // __cplusplus

#endif // DEFER_H_
