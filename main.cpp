#include <sstream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "glad.h"
#include <GLFW/glfw3.h>

#include "defer.h"
#include "Shader.h"

#define WINDOW_WIDTH  800
#define WINDOW_HEIGHT 600

static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0,0,width,height);
}

static void process_input(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, 1);
}

int main(void)
{

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    defer({ glfwTerminate(); });
   
    GLFWwindow* window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "learn-opengl", 0, 0);
    if (!window) return -1;
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) return -1;

    glViewport(0,0,WINDOW_WIDTH, WINDOW_HEIGHT);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    static float vertices[] = {
        // vertices         // colors
        -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
        0.5f,  -0.5f, 0.0f, 0.0f, 1.0f, 0.0f,
        0.5f,   0.5f, 0.0f, 0.0f, 0.0f, 1.0f
    };
    
    unsigned int vao;
    glGenVertexArrays(1, &vao);
    defer({ glDeleteVertexArrays(1, &vao); });
    glBindVertexArray(vao);

    unsigned int vbo;
    glGenBuffers(1, &vbo);
    defer({ glDeleteBuffers(1, &vbo); });

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), 0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*) (3*sizeof(float)));
    glEnableVertexAttribArray(1);

    Shader shader("./vertex.vert", "./fragment.frag");

    while(!glfwWindowShouldClose(window)) {
        process_input(window);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        shader.use();
        shader.set("dt", glfwGetTime());

        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0,3);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    return 0;
}

