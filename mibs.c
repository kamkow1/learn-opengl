#define MIBS_IMPL
#include "mibs.h"

Mibs_Default_Allocator alloc = mibs_make_default_allocator();

#define SRC \
    "main.cpp", \
    "Shader.cpp", \
    "glad.cpp"

bool build_learn_opengl(void)
{
    Mibs_Cmd cmd = {0};
    mdefer { mibs_da_deinit(&alloc, &cmd); }
    mibs_cmd_append(&alloc, &cmd, "g++", "-o", "learn-opengl", "-std=c++17");
    mibs_cmd_append(&alloc, &cmd, "-I./");
    mibs_cmd_append(&alloc, &cmd, SRC);
    mibs_cmd_append(&alloc, &cmd, "-lglfw", "-lGL", "-ldl");
    return mibs_run_cmd(&alloc, &cmd, MIBS_CMD_SYNC, 0).ok;
}

int main(int argc, char** argv)
{
    mibs_rebuild(&alloc, argc, argv);
    if (!build_learn_opengl()) return 1;

    return 0;
}

