#ifndef SHADER_H_
#define SHADER_H_

struct Shader {
    unsigned int id;

    Shader(const char* vert, const char* frag);
    ~Shader(void);
    void use(void);
    void set(const char* name, float f);
    void set(const char* name, double d);
    void set(const char* name, bool b);
    void set(const char* name, int i);
};

#endif // SHADER_H_
